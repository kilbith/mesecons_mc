local meseconspec_off = { conductor = {
	state = mesecon.state.off,
	onstate = "mesecons:wire_on"
}}

local meseconspec_on = { conductor = {
	state = mesecon.state.on,
	offstate = "mesecons:wire_off"
}}

local groups_on = {dig_immediate = 3, mesecon_conductor_craftable = 1, not_in_creative_inventory = 1, mesecons_content = 1}
local groups_off = {dig_immediate = 3, mesecon_conductor_craftable = 1, mesecons_content = 1}

local selectionbox = {
	type = "fixed",
	fixed = {-.5, -.5, -.5, .5, -.5+4/16, .5}
}

local tiles_off = { "mesecons_wire_off.png" }
local tiles_on = { "mesecons_wire_on.png" }

mesecon.register_node(":mesecons:wire", {
	description = "Mesecon",
	drawtype = "nodebox",
	inventory_image = "mesecons_wire_inv.png",
	wield_image = "mesecons_wire_inv.png",
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	selection_box = selectionbox,
	node_box = {
		type = "connected",
		fixed = {{-1/16, -1/2, -1/16, 1/16, -.5+1/16, 1/16}},
		connect_front = {{-1/16, -1/2, -1/2, 1/16, -.5+1/16, -1/16}},
		connect_left = {{-1/2, -1/2, -1/16, -1/16, -.5+1/16, 1/16}},
		connect_back = {{-1/16, -1/2, 1/16, 1/16, -.5+1/16, 1/2}},
		connect_right = {{1/16, -1/2, -1/16, 1/2, -.5+1/16, 1/16}},
		connect_top = {{-1/16, -1/2, -1/16, 1/16, .5, 1/16}},
	},
	connects_to = {"group:mesecons_content"},
	walkable = false,
	mesecon_wire = true,
	drop = "mesecons:wire_off"
},
{tiles = tiles_off, mesecons = meseconspec_off, groups = groups_off},
{tiles = tiles_on, mesecons = meseconspec_on, groups = groups_on})

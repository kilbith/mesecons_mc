--MESECON TORCHES

local rotate_torch_rules = function (rules, param2)
	if param2 == 5 then
		return mesecon.rotate_rules_right(rules)
	elseif param2 == 2 then
		return mesecon.rotate_rules_right(mesecon.rotate_rules_right(rules)) --180 degrees
	elseif param2 == 4 then
		return mesecon.rotate_rules_left(rules)
	elseif param2 == 1 then
		return mesecon.rotate_rules_down(rules)
	elseif param2 == 0 then
		return mesecon.rotate_rules_up(rules)
	else
		return rules
	end
end

local torch_get_output_rules = function(node)
	local rules = {
		{x = 1,  y = 0, z = 0},
		{x = 0,  y = 0, z = 1},
		{x = 0,  y = 0, z =-1},
		{x = 0,  y = 1, z = 0},
		{x = 0,  y =-1, z = 0}}

	return rotate_torch_rules(rules, node.param2)
end

local torch_get_input_rules = function(node)
	local rules = {{x = -2, y = 0, z = 0},
		       {x = -1, y = 1, z = 0}}

	return rotate_torch_rules(rules, node.param2)
end

minetest.register_craft({
	output = "mesecons_torch:mesecon_torch_on 4",
	recipe = {
		{"group:mesecon_conductor_craftable"},
		{"default:stick"},
	}
})

minetest.register_node("mesecons_torch:mesecon_torch_off", {
	drawtype = "mesh",
	mesh = "torch_floor.obj",
	tiles = {"redstone_torch_off.png"},
	inventory_image = "redstone_torch_off.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	walkable = false,
	selection_box = {
		type = "wallmounted",
		wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
	},
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1, mesecons_content = 1},
	drop = "mesecons_torch:mesecon_torch_on",
	mesecons = {receptor = {
		state = mesecon.state.off,
		rules = torch_get_output_rules
	}}
})

minetest.register_node("mesecons_torch:mesecon_torch_off_wall", {
	drawtype = "mesh",
	mesh = "torch_wall.obj",
	tiles = {{
		    name = "redstone_torch_off.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	light_source = 12,
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1},
	drop = "mesecons_torch:mesecon_torch_on",
	selection_box = {
		type = "wallmounted",
		wall_side = {-1/2, -1/2, -1/8, -1/8, 1/8, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
	mesecons = {receptor = {
		state = mesecon.state.off,
		rules = torch_get_output_rules
	}},
	drop = "mesecons_torch:mesecon_torch_on",
})

minetest.register_node("mesecons_torch:mesecon_torch_off_ceiling", {
	drawtype = "mesh",
	mesh = "torch_ceiling.obj",
	tiles = {{
		    name = "redstone_torch_off.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	light_source = 12,
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1, mesecons_content = 1},
	drop = "mesecons_torch:mesecon_torch_on",
	selection_box = {
		type = "wallmounted",
		wall_top = {-1/8, -1/16, -5/16, 1/8, 1/2, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
	mesecons = {receptor = {
		state = mesecon.state.off,
		rules = torch_get_output_rules
	}},
	drop = "mesecons_torch:mesecon_torch_on",
})




minetest.register_node("mesecons_torch:mesecon_torch_on", {
	description = "Redstone Torch",
	drawtype = "mesh",
	mesh = "torch_floor.obj",
	inventory_image = "redstone_torch_on.png",
	wield_image = "redstone_torch_on.png",
	tiles = {{
		    name = "redstone_torch_on.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 12,
	groups = {choppy=2, dig_immediate=3, flammable=1, attached_node=1, torch=1, mesecons_content = 1},
	drop = "default:torch",
	selection_box = {
		type = "wallmounted",
		wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
	on_place = function(itemstack, placer, pointed_thing)
		local under = pointed_thing.under
		local node = minetest.get_node(under)
		local def = minetest.registered_nodes[node.name]
		if def and def.on_rightclick and
			((not placer) or (placer and not placer:get_player_control().sneak)) then
			return def.on_rightclick(under, node, placer, itemstack,
				pointed_thing) or itemstack
		end

		local above = pointed_thing.above
		local wdir = minetest.dir_to_wallmounted(vector.subtract(under, above))
		local fakestack = itemstack
		if wdir == 0 then
			fakestack:set_name("mesecons_torch:mesecon_torch_on_ceiling")
		elseif wdir == 1 then
			fakestack:set_name("mesecons_torch:mesecon_torch_on")
		else
			fakestack:set_name("mesecons_torch:mesecon_torch_on_wall")
		end

		itemstack = minetest.item_place(fakestack, placer, pointed_thing, wdir)
		itemstack:set_name("mesecons_torch:mesecon_torch_on")

		return itemstack
	end,
	mesecons = {receptor = {
		state = mesecon.state.on,
		rules = torch_get_output_rules
	}},
})

minetest.register_node("mesecons_torch:mesecon_torch_on_wall", {
	drawtype = "mesh",
	mesh = "torch_wall.obj",
	tiles = {{
		    name = "redstone_torch_on.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	light_source = 12,
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1, mesecons_content = 1},
	drop = "mesecons_torch:mesecon_torch_on",
	selection_box = {
		type = "wallmounted",
		wall_side = {-1/2, -1/2, -1/8, -1/8, 1/8, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
	mesecons = {receptor = {
		state = mesecon.state.on,
		rules = torch_get_output_rules
	}},
	drop = "mesecons_torch:mesecon_torch_on",
})

minetest.register_node("mesecons_torch:mesecon_torch_on_ceiling", {
	drawtype = "mesh",
	mesh = "torch_ceiling.obj",
	tiles = {{
		    name = "redstone_torch_on.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	light_source = 12,
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1, mesecons_content = 1},
	drop = "mesecons_torch:mesecon_torch_on",
	selection_box = {
		type = "wallmounted",
		wall_top = {-1/8, -1/16, -5/16, 1/8, 1/2, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
	mesecons = {receptor = {
		state = mesecon.state.on,
		rules = torch_get_output_rules
	}},
	drop = "mesecons_torch:mesecon_torch_on",
})

minetest.register_abm({
	nodenames = {
		"mesecons_torch:mesecon_torch_off",
		"mesecons_torch:mesecon_torch_off_wall",
		"mesecons_torch:mesecon_torch_off_ceiling",
		"mesecons_torch:mesecon_torch_on",
		"mesecons_torch:mesecon_torch_on_wall",
		"mesecons_torch:mesecon_torch_on_ceiling",
	},
	interval = 1,
	chance = 1,
	action = function(pos, node)
		local is_powered = false
		for _, rule in ipairs(torch_get_input_rules(node)) do
			local src = vector.add(pos, rule)
			if mesecon.is_power_on(src) then
				is_powered = true
			end
		end

		if is_powered then
			if node.name == "mesecons_torch:mesecon_torch_on" then
				minetest.swap_node(pos, {name = "mesecons_torch:mesecon_torch_off", param2 = node.param2})
			elseif node.name == "mesecons_torch:mesecon_torch_on_wall" then
				minetest.swap_node(pos, {
					name = "mesecons_torch:mesecon_torch_off_wall", param2 = node.param2})
			elseif node.name == "mesecons_torch:mesecon_torch_on_ceiling" then
				minetest.swap_node(pos, {
					name = "mesecons_torch:mesecon_torch_off_ceiling", param2 = node.param2})
			end
			mesecon.receptor_off(pos, torch_get_output_rules(node))
		elseif node.name:find("mesecon_torch_off") then
			if node.name == "mesecons_torch:mesecon_torch_off" then
				minetest.swap_node(pos, {name = "mesecons_torch:mesecon_torch_on", param2 = node.param2})
			elseif node.name == "mesecons_torch:mesecon_torch_off_wall" then
				minetest.swap_node(pos, {
					name = "mesecons_torch:mesecon_torch_on_wall", param2 = node.param2})
			elseif node.name == "mesecons_torch:mesecon_torch_off_ceiling" then
				minetest.swap_node(pos, {
					name = "mesecons_torch:mesecon_torch_on_ceiling", param2 = node.param2})
			end
			mesecon.receptor_on(pos, torch_get_output_rules(node))
		end
	end
})

-- Param2 Table (Block Attached To)
-- 5 = z-1
-- 3 = x-1
-- 4 = z+1
-- 2 = x+1
-- 0 = y+1
-- 1 = y-1

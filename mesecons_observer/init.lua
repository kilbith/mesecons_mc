minetest.register_node(":mesecons:observer", {
	description = "Observer",
	is_ground_content = false,
	paramtype2 = "facedir",
	groups = {cracky=2, not_in_creative_inventory=1, mesecons_content = 1},
	tiles = {
		"observer_top.png", "observer_back_lit.png",
		"observer_side.png", "observer_side.png",
		"observer_front.png", "observer_back.png",
	},
	after_dig_node = function(pos, oldnode, oldmetadata, digger)
		local meta = minetest.get_meta(pos)
		local meta2 = meta
		meta:from_table(oldmetadata)
		local inv = meta:get_inventory()
		for i=1, inv:get_size("main") do
			local stack = inv:get_stack("main", i)
			if not stack:is_empty() then
				local p = {x=pos.x+math.random(0, 10)/10-0.5, y=pos.y, z=pos.z+math.random(0, 10)/10-0.5}
				minetest.add_item(p, stack)
			end
		end
		meta:from_table(meta2:to_table())
	end,
	-- TODO: Mesecons handling
	mesecons = {effector = {
	}}
})

